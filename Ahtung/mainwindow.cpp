#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QApplication::setStyle("Fusion");
    setDown();
    ui->le_pass->setEchoMode(QLineEdit::Password);
    ui->le_pass->setInputMethodHints(Qt::ImhHiddenText| Qt::ImhNoPredictiveText|Qt::ImhNoAutoUppercase);

    trayIcMenu = new QMenu(this);
    QAction *quitAction = new QAction("Закрыть", this);
    connect (quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    trayIcMenu->addAction(quitAction);
    sysTrayIc = new QSystemTrayIcon(this);
    QIcon icon(QIcon(":/new/prefix1/ico.ico"));
    sysTrayIc->setIcon(icon);
    sysTrayIc->setToolTip("Personal notes");
    sysTrayIc->setContextMenu(trayIcMenu);
    sysTrayIc->show();
    connect(sysTrayIc,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this,SLOT(showHide(QSystemTrayIcon::ActivationReason)));

    sysFile = new QFile();
    sysFile->setFileName(QDir::homePath().append("\\explorer.qres"));
    loadWnds();

    QTimer* tim = new QTimer(this);
    // yeah, its a dumb way, but it just works... (c) Todd Howard
    connect(tim, SIGNAL(timeout()), this, SLOT(onTimer()));
    tim->start(50);

}

static BOOL CALLBACK EnumWindowsProc(HWND hWnd, LPARAM lParam)
{
    // yeah, its abusing winAPI in a rough way, but it just works... (c) Todd Howard
    MainWindow * w = (MainWindow*)lParam;
    static quint32 counter = 0;

    QString wstr = "";
    if (IsWindowVisible(hWnd)) {

       char *buff = new char[255];
       GetWindowText(hWnd, (LPWSTR)&buff[0], 254);
       QString wstr = QString::fromWCharArray((LPWSTR)&buff[0]);
       delete []buff;

       //qDebug()<<wstr;
       if(!wstr.isEmpty()&&(wstr!=QString("Пуск"))&&(wstr!=QString("Program Manager"))){
           w->ui->cb_windows->addItem(wstr,counter);
           counter++;
       }
    }
    w->ui->cb_windows->setEditable(true);
    return TRUE;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_le_pass_returnPressed()
{
    if (ui->le_pass->text() == "112233")
        setUp();
    ui->le_pass->clear();
}

void MainWindow::showHide(QSystemTrayIcon::ActivationReason r)
{
    if (r == QSystemTrayIcon::DoubleClick)  {
        this->isVisible() ? this->hide() : this->showNormal();
    }
}

void MainWindow::setUp()
{
    this->setMaximumSize(QWIDGETSIZE_MAX,QWIDGETSIZE_MAX);
    this->setMinimumSize(0,0);
    ui->gb_setts->show();
    ui->pb_refresh->setEnabled(true);
}

void MainWindow::setDown()
{
    ui->gb_setts->hide();
    ui->pb_refresh->setEnabled(false);
    this->setFixedHeight(60);
}

void MainWindow::on_pb_save_clicked()
{
    saveWnds();
    setDown();
    showHide(QSystemTrayIcon::DoubleClick);
}

void MainWindow::on_pb_refresh_clicked()
{
    ui->cb_windows->clear();
    EnumWindows(EnumWindowsProc, (LPARAM)this);
}

void MainWindow::saveWnds()
{
    if (sysFile->open(QIODevice::WriteOnly)) {
      QDataStream asd(sysFile);
      QString tmp;
      for (int var = 0; var < ui->listWidget->count(); var++) {
          tmp = ui->listWidget->item(var)->text();
          asd << tmp;
      }
      sysFile->close();
    }
}

void MainWindow::loadWnds()
{
    if (sysFile->open(QIODevice::ReadOnly)) {
        ui->listWidget->clear();
        QDataStream asd(sysFile);
        QString tmp;
        do {
            asd >> tmp;
            ui->listWidget->addItem(tmp);
        } while (!asd.atEnd());
        sysFile->close();
    }
}

void MainWindow::on_pb_del_clicked()
{
    ui->listWidget->takeItem(ui->listWidget->currentRow());
}

void MainWindow::on_pb_add_clicked()
{
    ui->listWidget->addItem(ui->cb_windows->currentText());
}

void MainWindow::hideWindow(bool hide)
{
    HWND wndID;
    LPCTSTR title;
    for (int var = 0; var < ui->listWidget->count(); var++) {
        QString asd = ui->listWidget->item(var)->text();
        title = asd.toStdWString().c_str();
        wndID = FindWindow(nullptr, title);
        if (hide)
            ShowWindow(wndID, SW_HIDE);
        else
            ShowWindow(wndID, SW_SHOW);
    }
}

void MainWindow::on_pb_hide_clicked(bool checked)
{
    hideWindow(checked);
}

void MainWindow::onTimer()
{
    // C-style code, yey!
    static bool filter = false;
    static quint32 filt_cou = 0;
    if (filter) {
        filt_cou++;
        if (filt_cou > 19) {
            filt_cou = 0;
            filter = false;
        }
    } else {
        if ((GetKeyState(VK_CONTROL)<0) && (GetKeyState(VK_OEM_3)<0))
            {
                 hideWindow(false); //show
            }
        if ((GetKeyState(VK_OEM_3)<0) && (GetKeyState(VK_CONTROL)>=0))
            {
                 hideWindow(true); //hide
            }
    }
}
