#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "windows.h"
#include <QMenu>
#include <QSystemTrayIcon>
#include <QDir>
#include <QFile>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    Ui::MainWindow *ui;
private slots:

    void on_le_pass_returnPressed();
    void showHide(QSystemTrayIcon::ActivationReason r);
    void setUp();
    void setDown();
    void on_pb_save_clicked();
    void on_pb_refresh_clicked();
    void saveWnds();
    void loadWnds();
    void on_pb_del_clicked();
    void on_pb_add_clicked();
    void hideWindow(bool hide);
    void on_pb_hide_clicked(bool checked);
    void onTimer();

private:
    QFile *sysFile;
    QSystemTrayIcon *sysTrayIc;
    QMenu *trayIcMenu;
};

#endif // MAINWINDOW_H
